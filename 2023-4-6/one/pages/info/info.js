// pages/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.request({
      url: 'https://api.uomg.com/api/rand.qinghua?format', //仅为示例，并非真实的接口地址
      data: {
        format: 'json',
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 成功的回调函数 
      success: (res)=> {
        let content = res.data.content
        this.setData({
          // 将数据放到text中
          text:content
        })
        // 异步放到本地存储中
        wx.setStorage({
          key:'one',
          data : res.data.content
        })
      }
    })


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})