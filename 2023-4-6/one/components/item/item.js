// components/item/item.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 在此接收父传来的值
    title:{
      type:String,  //传值的数据类型 
      value:' 没有写的什么是我'  //默认值 
    },
    count:Number

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
// 点击加1
add(){
  // console.log(this,1);
  this.setData({
    count:this.data.count +1
  })
}
  }
})
