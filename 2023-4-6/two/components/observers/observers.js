// components/observers/observers.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    user: {
      name: '哈哈',
      age: '14'
    },

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 子传父
    click() {
      this.triggerEvent('b','123')
      // this.triggerEvent('fn','111')
    },
    // 点击修改并监听
    handleClick() {
      this.setData({
        user: {
          name: '一二',
          age: '43'
        }
      })
    },
  }
})