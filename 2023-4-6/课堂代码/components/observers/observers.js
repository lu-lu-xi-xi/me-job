// components/observers/observers.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    user: {
      name: "小牛",
      age: "11"
    }
  },
  // 监听组件数组变化 
  observers: {
    'user.name,user.age'(val1, val2) {
      console.log(val1, val2, '11');
    }
  },

  methods: {
    // 子传父的参数
    // 第一个是名，第二个是传的内容
    tag() {
      this.triggerEvent('a', '123')
    },
    //监听数据的变化 
    handleClick() {
      this.setData({
        user :{
          name: '大牛',
          age:8
        }
      })
    }
  }
})