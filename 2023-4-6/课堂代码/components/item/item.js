// components/item/item.js
Component({
  
  // 设置样式的隔离效果
  options:{
    // styleIsolation:'apply-shared',
    // styleIsolation:'isolated'
  },

  properties: {
    title:{
      type:String,
      value:"默认"
    },
    abc:{
      type:Number,
      value:"默认的组件"
    },
    text:{
     type:String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    add(){
      // console.log(this,11);
      this.setData({
        abc:this.data.abc + 1
      })
    },
  }
})
