// pages/info/info.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
   lists :[],
   text:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 普通用法
    wx.request({
      url: 'http://129.211.169.131:13688/movie/citys', //仅为示例，并非真实的接口地址
      data: {
        // 放的是发送的时候带的参数
        // 名字: '数据'
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 成功的回调函数
      success: (res) =>{
        // 返回的数据 
       let list = res.data
      // 将返回值放到lists中
       this.setData({
         lists:list,
        })
      }
    })
    
    wx.request({
      url: 'https://api.uomg.com/api/rand.qinghua?format', //仅为示例，并非真实的接口地址
      method:"POST", //请求方式
      data: {
         // 放的是发送的时候带的参数
        // 名字: '数据'
        format: 'json',
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      // 成功的回调函数 
      success: (res)=> {
        let content = res.data.content
        this.setData({
          // 将数据放到text中
          text:content
        })
        // 异步放到本地存储中
        wx.setStorage({
          key:'one',
          data : res.data.content
        })
        // 同步存储数据
        try{
          //第一个key名字，第二个是要存的值  
          wx.setStorageSync('info',res.data.content)
        }catch(error){}
        // 从本地存储中取出来
        // wx.setStorage({
        //   key:'info',
        //   success(res){
        //     console.log('本地存储中的',res);
        //   }
        // })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
   
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})