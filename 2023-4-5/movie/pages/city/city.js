
import {
    getCitys
} from '../../api/api'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cityList: [],
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        getCitys().then(res => {
            let datas = res
            console.log(datas, 'citys');
            datas = datas.sort((a, b) => a.city_pre.localeCompare(b.city_pre))
            datas.forEach(item => {
                item.city_pre = item.city_pre.toUpperCase()
            })
            let result = []
            datas.forEach(item => {
                if (result.every(v => v.city_pre !== item.city_pre)) {
                    result.push({
                        city_pre: item.city_pre
                    })
                }
            })
            
            result.forEach(item => {
                item.children = []
                datas.forEach(v => {
                    if(item.city_pre === v.city_pre){
                        item.children.push(v.city_name)
                    }
                })
            })
            console.log(result, 'result');
            this.setData({
                cityList: result
            })
        })
    },
})