import fly from '../utils/request'

/**
 * 1.根据城市获取今日上映电影列表
 * @param {Object} data 请求参数
 * @param {Object} options 配置项，默认为空对象
 */
export function getMovie(data,options = {}){
    return fly.request({
      url: '/movie/today ',data,options})
}

/**
 * 2.根据电影id获取详情
 * @param {Object} data 请求参数
 * @param {Object} options 配置项，默认为空对象
 */
export function getDetail(data,options={}){
    return fly.request('/movie/query',data,options)
}

/**
 * 3.获取所有城市列表 没有请求参数
 *@param {Object} data 请求参数
 * @param {Object} options 配置项，默认为空对象
 */
export function getCitys(options={}){
    return fly.request('/movie/citys',options)
}