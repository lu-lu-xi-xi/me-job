// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  // 点击跳转
  handleClick() {
// 跳转到tabbar页面，关闭其他所有非tabbar页面
    // wx.switchTab({
    //   url: "/pages/one/one"
    // })
   // 关闭所有页面，跳转到应用内的某个页面
    wx.reLaunch({
      url: '/pages/one/one',
    })
    // 关闭当前页面，跳转到应用内的某个页面，无法跳到tabbar页面
    // wx.redirectTo({
    //   url: '/pages/one/one',
    // })
  },
  

  data: {
    animationData:{},
    images: [
      '../img/1.webp',
      '../img/2.webp',
      '../img/3.webp',
      '../img/4.webp'
    ]
  },
 // 动画
 onShow: function(){
  var animation = wx.createAnimation({
    duration: 2000,
    timingFunction: 'ease-in-out',
  })
  animation.skew(10, 10).scale(2,3).rotate(45).step()
  this.setData({
    animationData:animation.export()
  })

  setTimeout(function() {
    animation.translate(100).step()
    this.setData({
      animationData:animation.export()
    })
  }.bind(this), 1000)
},
  // 点击事件
  // 具备交互行为的模态框
  handleClicks() {
    wx.showModal({
      title: '确认',
      content: '是否确认删除？删除后将无法恢复',
      // editable: true
      // 确认
      success(res) {
        console.log(res, '按钮操作')
      },
      // 取消
      fail(err) {
        console.log(err, '报错的')
      }
    })
    // loading动画，主动开启，也需要主动关闭
    // 也就是加载用
    wx.showLoading({
      title: '正在玩命加载中',
    })
    setTimeout(() => wx.hideLoading(), 2000)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})