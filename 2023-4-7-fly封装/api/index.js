
// 配置的是请求的东西
import fly from '../utils/request'
/**
 * 
 * @param {object} data 请求参数b 
 * @param {object} options  配置项，默认为空
 */
export function getQQInfo(data,options = {}){
  //                 请求的路径       请求参数     配置项，默认为空
  return fly.request('/api/qq.info', data,       options)
}