// fly的封装

// 导入fly
import Fly from '../lib/wx'
// 获取 fly 的实例
const fly = new Fly

fly.config.baseURL = "https://api.uomg.com" //设置请求的基础地址
fly.config.timeout = 10000 //设置的请求超时时间
// 请求拦截器
fly.interceptors.request.use(config => {
  // 这里会有一个token的认证
  // if(token) fly.config.headers.token = token
  // 加载动画
  wx.showLoading({
    title: '我在加载没有卡',
  })
  return config
}, err => {
  // 报错信息
  return Poimise.reject(err)
})
//响应拦截 
fly.interceptors.response.use(response => {
  // 隐藏动画
  wx.hideLoading()
  // 根据后台接口状态码反馈判断各种异常信息，如token过期等
  let title = '未知错误'
  let icon = 'error'
  return response.data
}, err => {
  // 根据浏览器返回的异常状态码判断各种场景，如网络错误
  return Poimise.reject(err)
})

export default fly 