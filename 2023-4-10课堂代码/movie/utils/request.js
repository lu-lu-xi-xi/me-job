import Fly from '../lib/wx'
const fly = new Fly()
// 请求基础地址
fly.config.baseURL = 'http://129.211.169.131:13688'
// 设置请求超时时间
fly.config.timeout = 10000;
// 请求拦截器
fly.interceptors.request.use(config => {
    // 请求发出前显示loading动画
    wx.showLoading({
        title: '正在加载中',
    })
    return config

}, err => {
    return Promise.reject(err)
})
// 响应拦截器
fly.interceptors.response.use(response => {
    // 根据后台接口返回的状态码判断异常场景
    let title = '未知错误'
    let icon = 'error'
    console.log(response, 'res');
    
    wx.showToast({
        title,
        icon
    })
    return response.data
}, err => {
    // 根据浏览器异常状态码判断各种异常场景，比如网络错误
    let title = '未知错误'
   
    wx.showToast({
        title,
        icon: 'error'
    })
    return Promise.reject(err)
})

export default fly