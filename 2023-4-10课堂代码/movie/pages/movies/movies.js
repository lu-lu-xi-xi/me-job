import { getMovie } from '../../api/api'

Page({
  data: {
    movieLsit:[]
  },
  onLoad:function(options){
  
    getMovie({
        id:options.id
    }).then(res =>{
     
        this.setData({
            movieLsit : res.data
        })
    })
},
// 路由跳转
gotoDetail(e){
console.log(e.currentTarget.dataset);
let id = e.currentTarget.dataset.id;
wx.navigateTo({
  url: `../detail/detail?id=${id}`
})
}
})
