import { getDetail } from '../../api/api'

Page({
  data: {
    detail:[]
  },
  onLoad(options){
    
    getDetail({
        movieid:options.id
    }).then(res =>{
        this.setData({
            detail : res.data
        })
    })
}
})
