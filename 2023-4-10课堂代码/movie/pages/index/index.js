// index.js
// 获取应用实例
const app = getApp()
    
Page({
    // 城市
    handleCity(){
        console.log(111);
        wx.redirectTo({
            url: '/pages/city/city',
          })
    },
    // 影片
    handleClick() {      
        
        wx.redirectTo({
              url: '/pages/movies/movies',
            })
     },

  data: {
    images: [
        '../img/1.webp',
        '../img/2.webp',
        '../img/3.webp',
        '../img/4.webp'
      ],
        name:'成都市'
  },
  // 事件处理函数
  bindViewTap() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad(options) {
    //   console.log('name',options.name);
      this.setData({
          name : options.name
      })
  },
})
