// pages/video/video.js
Page({
 
  data: {
    createVideoContext :null, //视频实例
    src:"" ,//视频地址
    danmuList:[ //弹幕列表
      {text:'first',color:'#ff0000',time:1 },
      {text:'second',color:'#008080',time:2 },
      {text:'three',color:'#ff00ff',time:3 },  
    ],
    inputValue:'',//弹幕文本
  },
  // 获取视频 
  video(){
    wx.chooseMedia({
      sourceType: ['album', 'camera'],
      maxDuration: 10,
      camera: 'back',
      success :res=> {
        console.log(res)
        this.setData({
          src : res.tempFiles[0].tempFilePath
        })
      }
    })
  },
  // 视频播放
  audioPlay : function() {
    this.audioCtx.play()
  },
  audioPause(){
    this.audioCtx.pause()
  },
  onLoad(options) {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
      this.videoContext = wx.createVideoContext('myVideo')

      this.audioCtx = wx.createAudioContext('myAudio')

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})