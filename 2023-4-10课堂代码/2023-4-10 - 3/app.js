// app.js
App({
  onLaunch() {
    // 生命周期，小程序初始化时会调用，在这里可以判断用户是否登录，如已经登录了就跳转至首页
    try{
      const openid = wx.getStorageSync('openid')
      console.log(openid);
      // 已经登录,跳到video
      if(openid){
        wx.navigateTo({
            url: '/pages/video/video',
          })
      } else {
        // 没有登录 
        wx.navigateTo({
          url: '/pages/logs/logs',
        })
      }
    }catch (e){

    }
  }

})
